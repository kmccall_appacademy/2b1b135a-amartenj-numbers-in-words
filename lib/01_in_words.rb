class Fixnum

  def in_words
    return "zero" if self == 0

    powers = {
      2 => "thousand",
      3 => "million",
      4 => "billion",
      5 => "trillion"
    }

    blocks = break_into_blocks(self)

    words = []

    blocks.each.with_index do |block, idx|
      power = blocks.length - idx
      decomp = decompose_three_digits(block)
      words << decomp if block != 0
      if block != 0
        words << powers[power] if power > 1
      end
    end

    words.join(" ")
  end

  def break_into_blocks(num)
    return [0] if num == 0

    sections = []

    while num != 0
      sections.unshift(num % 1000)
      num /= 1000
    end

    sections
  end

  def decompose_three_digits(digits)
    map = {
      90 => "ninety",
      80 => "eighty",
      70 => "seventy",
      60 => "sixty",
      50 => "fifty",
      40 => "forty",
      30 => "thirty",
      20 => "twenty",
      19 => "nineteen",
      18 => "eighteen",
      17 => "seventeen",
      16 => "sixteen",
      15 => "fifteen",
      14 => "fourteen",
      13 => "thirteen",
      12 => "twelve",
      11 => "eleven",
      10 => "ten",
      9 => "nine",
      8 => "eight",
      7 => "seven",
      6 => "six",
      5 => "five",
      4 => "four",
      3 => "three",
      2 => "two",
      1 => "one",
      0 => "zero"
    }

    return map[digits] if map[digits]

    res = []

    tens_ones = digits % 100
    hundreds = digits / 100 % 10

    if hundreds != 0
      res << map[hundreds] + " hundred"
    end

    # debugger

    if tens_ones != 0
      if map[tens_ones]
        res << map[tens_ones]
      else
        tens = (tens_ones / 10) * 10
        ones = tens_ones % 10

        if tens != 0
          res << map[tens]
        end

        if ones != 0
          res << map[ones]
        end
      end
    end

    p res.join(" ")
  end

end
